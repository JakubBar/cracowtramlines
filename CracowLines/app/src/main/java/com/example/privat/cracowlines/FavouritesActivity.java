package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Map;

/**
 * Created by Privat on 31.12.2017.
 */

public class FavouritesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_activity);

        ListView listItem = (ListView)findViewById(R.id.list_item2);
        ImageView back_array = (ImageView)findViewById(R.id.image_back_title);
        ((TextView)findViewById(R.id.text_title)).setText("FAVOURITES STOPS");
        ImageView refresh_activity = (ImageView)findViewById(R.id.image_refresh_activity);
        refresh_activity.setBackgroundResource(0);
        ImageView home_button = (ImageView)findViewById(R.id.image_home_button);
        home_button.setBackgroundResource(0);

        DBHelper dbHelper = new DBHelper(this);
        new FavouritesTask(getApplicationContext(),listItem,dbHelper.getAllFavouritesStops(),FavouritesActivity.this).execute();

        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                String number_item = ((Map<String, String>)parent.getItemAtPosition(position)).get("number");
                Intent intent2 = new Intent(FavouritesActivity.this, StopSelectedActivity.class);
                intent2.putExtra("number",number_item);
                startActivityForResult(intent2, 1);

            }
        });

        back_array.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }
}