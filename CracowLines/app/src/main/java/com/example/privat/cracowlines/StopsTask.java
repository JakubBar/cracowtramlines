package com.example.privat.cracowlines;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Privat on 27.12.2017.
 */

public class StopsTask extends AsyncTask<Void, String, Void> {
    private Context context;
    private ListView lv;
    private GridView gv;
    private Activity ac;
    private ProgressDialog progressDialog ;
    private Boolean dismiss = true;
    ArrayList<HashMap<String, String>> stopsList;

    StopsTask(Context _context, GridView _gv, Activity _ac) {
        context = _context;
        gv = _gv;
        ac=_ac;
        stopsList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(ac);
        progressDialog.setTitle("Downloading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler(context);
        if(!sh.isNetworkAvailable()){
            publishProgress("Network is unavailable.");
            dismiss=false;
        }
        String url = "http://www.ttss.krakow.pl/internetservice/geoserviceDispatcher/services/stopinfo/stops?left=-648000000&bottom=-324000000&right=648000000&top=324000000";
        String jsonStr = sh.makeServiceCall(url);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                // Getting JSON Array node
                JSONArray stopsJSON = jsonObj.getJSONArray("stops");
                for (int i = 0; i < stopsJSON.length(); i++) {
                    JSONObject c = stopsJSON.getJSONObject(i);
                   // String id = c.getString("id");
                    String name = c.getString("name");
                    String shortName = c.getString("shortName");
                    HashMap<String, String> stops = new HashMap<>();
                   // stops.put("id", id);
                    stops.put("name", name);
                    stops.put("number", shortName);
                    stopsList.add(stops);
                }
            } catch (final JSONException e) {
            }

        } else {
        }
        Collections.sort(stopsList, new MapComparator("name",false));
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        progressDialog.setTitle("Error");

        progressDialog.setMessage(values[0]);

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing() && dismiss) {
            progressDialog.dismiss();
        }
        gv.setNumColumns(2);
        ListAdapter adapter = new com.example.privat.cracowlines.ListAdapter(context, stopsList,
                R.layout.stops_list_item, new String[]{"number", "name","number"},
                new int[]{R.id.id_stops, R.id.name_stops,R.id.favourites},ac);
        gv.setAdapter(adapter);

    }
}