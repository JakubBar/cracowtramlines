package com.example.privat.cracowlines;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Privat on 31.12.2017.
 */

public class FavouritesTask extends AsyncTask<Void, String, Void> {
    private Context context;
    private ListView lv;
    private List<String> favStops;
    private Activity ac;
    private ProgressDialog progressDialog ;
    ArrayList<HashMap<String, String>> stopsList;
    private Boolean dismiss = true;

    FavouritesTask(Context _context, ListView _lv,List<String> _favStops, Activity _ac) {
        context = _context;
        favStops = _favStops;
        lv = _lv;
        ac = _ac;
        stopsList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(ac);
        progressDialog.setTitle("Downloading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler(context);
        if(!sh.isNetworkAvailable()){
            publishProgress("Network is unavailable.");
            dismiss=false;
        }
        for(int j=0 ; j < favStops.size() ; j++){
            String url = "http://www.ttss.krakow.pl/internetservice/services/stopInfo/stop?stop=" + favStops.get(j);
            String jsonStr = sh.makeServiceCall(url);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String name = jsonObj.getString("passengerName");
                    HashMap<String, String> stops = new HashMap<>();
                    stops.put("name", name);
                    stops.put("number", favStops.get(j));
                    stopsList.add(stops);

                } catch (final JSONException e) {
                }

            } else {}
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        progressDialog.setTitle("Error");

        progressDialog.setMessage(values[0]);

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing() && dismiss) {
            progressDialog.dismiss();
        }
        android.widget.ListAdapter adapter = new SimpleAdapter(context, stopsList,
                R.layout.stops_list_item, new String[]{"number", "name"},
                new int[]{R.id.id_stops, R.id.name_stops});
        lv.setAdapter(adapter);
    }
}