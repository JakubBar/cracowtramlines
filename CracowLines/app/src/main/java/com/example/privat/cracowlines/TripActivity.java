package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TextView;

/**
 * Created by Privat on 29.12.2017.
 */

public class TripActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_activity);

        TableLayout tableTrip= (TableLayout) findViewById(R.id.tableTrip);
        ImageView back_array = (ImageView)findViewById(R.id.image_back_title);
        ImageView refresh_activity = (ImageView)findViewById(R.id.image_refresh_activity);
        ImageView home_button = (ImageView)findViewById(R.id.image_home_button);
        ((TextView)findViewById(R.id.text_title)).setText("AVAILABLE STOPS" );

        new TripTask(getApplicationContext(),tableTrip,getIntent().getExtras().get("tripId").toString(),this,"arrival").execute();

        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripActivity.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                startActivityForResult(intent, 1);
            }
        });

        back_array.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
        refresh_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition( 0, 0);
                startActivity(getIntent());
                overridePendingTransition( 0, 0);
            }
        });
    }
}