package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);
        Button b_lines = (Button) findViewById(R.id.button_lines);
        Button b_stops = (Button) findViewById(R.id.button_stops);
        ImageView iv = (ImageView)findViewById(R.id.favourites_main) ;
        final GridView gv = (GridView) findViewById(R.id.gridView);
        ((TextView)findViewById(R.id.text_title)).setText("LINES AND STOPS");
        ImageView back_array = (ImageView)findViewById(R.id.image_back_title);
        back_array.setBackgroundResource(0);
        ImageView refresh_activity = (ImageView)findViewById(R.id.image_refresh_activity);
        refresh_activity.setBackgroundResource(0);
        ImageView home_button = (ImageView)findViewById(R.id.image_home_button);
        home_button.setBackgroundResource(0);

        b_stops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new StopsTask(getApplicationContext(),gv,MainActivity.this).execute();
            }
        });

        b_lines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LinesTask(getApplicationContext(),gv,MainActivity.this).execute();
            }
        });

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FavouritesActivity.class);
                startActivityForResult(intent, 1);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultsCode, Intent data){
        if(requestCode ==1)
        {
            if(resultsCode == Activity.RESULT_OK){}
            if(resultsCode == Activity.RESULT_CANCELED){}
        }
    }
}
