package com.example.privat.cracowlines;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Privat on 27.12.2017.
 */

public class LinesTask extends AsyncTask<Void, String, Void> {
    private Context context;
    private ListView lv;
    private GridView gv;
    ArrayList<HashMap<String, String>> linesList;
    private ProgressDialog progressDialog ;
    private Activity ac;
    private Boolean dismiss = true;

    LinesTask(Context _context, GridView _gv, Activity _ac) {
        context = _context;
        gv = _gv;
        ac = _ac;
        linesList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(ac);
        progressDialog.setTitle("Downloading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();

    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler(context);
        if(!sh.isNetworkAvailable()){
            publishProgress("Network is unavailable.");
            dismiss=false;
        }
        for(int j=1;j < 10; j++){
            String url = "http://www.ttss.krakow.pl/internetservice/services/lookup/autocomplete/json?query=" + j;
            String jsonStr = sh.makeServiceCall(url);
            if (jsonStr != null) {
                try {
                    JSONArray jsonObj = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject c = jsonObj.getJSONObject(i);
                        String name = c.getString("name");
                        if(name.startsWith("MPK ")){
                            String id = c.getString("id");
                            HashMap<String, String> lines = new HashMap<>();
                            lines.put("id", id);
                            lines.put("name", name.substring(name.indexOf("MPK ")+4));
                            linesList.add(lines);
                        }
                }
                } catch (final JSONException e) {
                }

            } else {
            }
        }
        Collections.sort(linesList, new MapComparator("name",true));
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        progressDialog.setTitle("Error");

        progressDialog.setMessage(values[0]);

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing() && dismiss) {
            progressDialog.dismiss();
        }
        gv.setNumColumns(3);
        ListAdapter adapter = new com.example.privat.cracowlines.ListAdapter(context, linesList,
                R.layout.lines_list_item, new String[]{"id", "name"},
                new int[]{R.id.id_stops, R.id.name_lines},ac);
        gv.setAdapter(adapter);
    }
}