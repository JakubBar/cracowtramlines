package com.example.privat.cracowlines;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.TableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Privat on 29.12.2017.
 */

public class TripTask extends AsyncTask<Void, String, Void> {
    private Context context;
    private TableLayout tl;
    private String tripId;
    private Activity ac;
    private String mode;
    private ProgressDialog progressDialog ;
    ArrayList<HashMap<String, String>> oldStopsList;
    ArrayList<HashMap<String, String>> actualStopsList;
    ArrayList<HashMap<String, String>> infoList;
    private Boolean dismiss = true;

    TripTask(Context _context, TableLayout _tl, String _tripId, Activity _ac, String _mode) {
        context = _context;
        tripId = _tripId;
        tl = _tl;
        ac= _ac;
        mode = _mode;
        oldStopsList = new ArrayList<>();
        actualStopsList = new ArrayList<>();
        infoList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(ac);
        progressDialog.setTitle("Downloading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler(context);
        if(!sh.isNetworkAvailable()){
            publishProgress("Network is unavailable.");
            dismiss=false;
        }
        String url = "http://www.ttss.krakow.pl/internetservice/services/tripInfo/tripPassages?mode="+mode+"&tripId="+tripId;
        String jsonStr = sh.makeServiceCall(url);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                HashMap<String, String> info = new HashMap<>();
                info.put("routeName", jsonObj.getString("routeName"));
                info.put("directionText", jsonObj.getString("directionText"));
                infoList.add(info);
                JSONArray actualJSON = jsonObj.getJSONArray("actual");
                for (int i = 0; i < actualJSON.length(); i++) {
                    JSONObject c = actualJSON.getJSONObject(i);
                    HashMap<String, String> actuals = new HashMap<>();
                    actuals.put("actualTime", c.has("actualTime") ? c.getString("actualTime") : c.getString("plannedTime"));
                    actuals.put("nameStop", c.getJSONObject("stop").get("name").toString());
                    actuals.put("number", c.getJSONObject("stop").get("shortName").toString());
                    actuals.put("status", c.getString("status"));
                   // actuals.put("mode",mode);
                    //actuals.put("tripId",tripId);
                    actualStopsList.add(actuals);
                }
                JSONArray oldJSON = jsonObj.getJSONArray("old");
                for (int i = 0; i < oldJSON.length(); i++) {
                    JSONObject c = oldJSON.getJSONObject(i);
                    HashMap<String, String> olds = new HashMap<>();
                    olds.put("actualTime", "x");
                    olds.put("nameStop", c.getJSONObject("stop").get("name").toString());
                    olds.put("number", c.getJSONObject("stop").get("shortName").toString());
                    oldStopsList.add(olds);
                }


            } catch (final JSONException e) {
            }

        } else {}
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        progressDialog.setTitle("Error");

        progressDialog.setMessage(values[0]);

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing() && dismiss) {
            progressDialog.dismiss();
        }
        Random rm = new Random();
        tl.removeAllViews();
        CreateTable ct = new CreateTable(context,rm,mode,tl,ac,infoList,actualStopsList,oldStopsList);
        ct.getTripHeader();
        ct.getTripBody();
    }
}

