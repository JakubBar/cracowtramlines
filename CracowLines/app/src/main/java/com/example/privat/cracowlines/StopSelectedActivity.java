package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

/**
 * Created by Privat on 28.12.2017.
 */

public class StopSelectedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stops_selected_activity);

        TableLayout tableStops= (TableLayout) findViewById(R.id.tableStops);
        ImageView back_array = (ImageView)findViewById(R.id.image_back_title);
        ((TextView)findViewById(R.id.text_title)).setText("AVAILABLE LINES");
        ImageView refresh_activity = (ImageView)findViewById(R.id.image_refresh_activity);
        ImageView home_button = (ImageView)findViewById(R.id.image_home_button);

        new StopSelectedTask(getApplicationContext(),tableStops,getIntent().getExtras().get("number").toString(),this,"arrival",getIntent().getExtras().get("nameLine") != null ? getIntent().getExtras().get("nameLine").toString() : "").execute();

        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StopSelectedActivity.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                startActivityForResult(intent, 1);
            }
        });

        back_array.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        refresh_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition( 0, 0);
                startActivity(getIntent());
                overridePendingTransition( 0, 0);
            }
        });

    }


}
