package com.example.privat.cracowlines;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Privat on 31.12.2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "favourites_stops.db";
    private static final String TABLE_CONTACT = "favourites";
    private static final String KEY_ID = "id";
    private static final String KEY_NUMBER = "stop_number";
    private static final String KEY_ANDROID = "android_id";
    private String aid;
    private Context con;

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        con = context;
        aid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACT + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NUMBER + " TEXT, "+
                KEY_ANDROID + " TEXT )" ;
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Toast.makeText(con.getApplicationContext(),
                "Aktualizacja bazy danych z wersji " + oldVersion + " do "+ newVersion,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        Toast.makeText(con.getApplicationContext(),
                "Aktualizacja bazy danych z wersji " + oldVersion + " do "+ newVersion,
                Toast.LENGTH_LONG).show();
    }

    public void insertFavouritesStop (String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NUMBER, number);
        contentValues.put(KEY_ANDROID, aid);
        db.insert(TABLE_CONTACT, null, contentValues);
        db.close();
    }

    public List<String> getAllFavouritesStops() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<String> fList = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from "+TABLE_CONTACT, null);

        if(cursor.moveToLast()) {
            do {
                fList.add(cursor.getString(1));
            } while (cursor.moveToPrevious());
        }
        cursor.close();
        db.close();
        return fList;
    }

    public boolean checkFavouritesStops(String number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CONTACT,
                new String[] { KEY_ID, KEY_NUMBER, KEY_ANDROID }, KEY_NUMBER + "=?", new String[] { number }, null, null, null);

        return cursor.getCount() == 0 ? false : true;
    }

    public int getFavouritesStopsCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CONTACT,new String[]{"COUNT(" + KEY_ID + ") AS count"}, null,null,null,null,null);

        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        return 0;
    }

    public void deleteFavouritesStop(String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACT, KEY_NUMBER +"=?", new String[]{number});
        db.close();
    }
}