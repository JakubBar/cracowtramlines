package com.example.privat.cracowlines;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Random;

/**
 * Created by Privat on 28.12.2017.
 */

public class StopSelectedTask extends AsyncTask<Void, String, Void> {
    private Context context;
    private TableLayout tl;
    private String number;
    private Activity ac;
    private String nameLineSelected;
    private String mode;
    private String mixedTime;
    private ProgressDialog progressDialog ;
    private Boolean dismiss = true;
    ArrayList<HashMap<String, String>> departuresList;

    StopSelectedTask(Context _context, TableLayout _tl, String _number, Activity _ac, String _mode,String _nameLineSelected) {
        context = _context;
        number = _number;
        tl = _tl;
        ac= _ac;
        mode = _mode;
        nameLineSelected= _nameLineSelected;
        departuresList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(ac);
        progressDialog.setTitle("Downloading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        HttpHandler sh = new HttpHandler(context);
        if(!sh.isNetworkAvailable()){
            publishProgress("Network is unavailable.");
            dismiss=false;
        }
        String url = "http://www.ttss.krakow.pl/internetservice/services/passageInfo/stopPassages/stop?stop=" + number + "&mode="+mode;
        String jsonStr = sh.makeServiceCall(url);
        if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONArray stopsJSON = jsonObj.getJSONArray("actual");
                    for (int i = 0; i < stopsJSON.length(); i++) {
                        JSONObject c = stopsJSON.getJSONObject(i);
                        if(!c.getString("patternText").equals(nameLineSelected) && !nameLineSelected.equals("")) continue;
                        HashMap<String, String> stops = new HashMap<>();
                        stops.put("line", c.getString("patternText"));
                        stops.put("direction", c.getString("direction"));
                        stops.put("time", c.getString("plannedTime"));
                        stops.put("tripId", c.getString("tripId"));
                        departuresList.add(stops);
                    }
                } catch (final JSONException e) {
                }

        } else {}
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        progressDialog.setTitle("Error");

        progressDialog.setMessage(values[0]);

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing() && dismiss) {
            progressDialog.dismiss();
        }
        Random rm = new Random();
        tl.removeAllViews();
        CreateTable ct = new CreateTable(context,rm,mode,tl,departuresList,nameLineSelected,ac);
        ct.getHeader();
        ct.getBody();
    }
}
