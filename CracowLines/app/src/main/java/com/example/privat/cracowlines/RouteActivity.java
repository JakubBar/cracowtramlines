package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Map;

/**
 * Created by Privat on 28.12.2017.
 */

public class RouteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_activity);
        ListView listItem = (ListView)findViewById(R.id.list_item2);

        ImageView back_array = (ImageView)findViewById(R.id.image_back_title);
        String nameLine  = getIntent().getExtras().get("nameLine").toString();
        ((TextView)findViewById(R.id.text_title)).setText("ROUTE LINE " + nameLine);
        ImageView refresh_activity = (ImageView)findViewById(R.id.image_refresh_activity);
        refresh_activity.setBackgroundResource(0);
        ImageView home_button = (ImageView)findViewById(R.id.image_home_button);
        home_button.setBackgroundResource(0);

        new RouteTask(getApplicationContext(),listItem,getIntent().getExtras().get("routeId").toString(),nameLine,RouteActivity.this).execute();

        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                String number_item = ((Map<String, String>)parent.getItemAtPosition(position)).get("number");
                String name_line = ((Map<String, String>)parent.getItemAtPosition(position)).get("nameLineSelected");
                Intent intent2 = new Intent(RouteActivity.this, StopSelectedActivity.class);
                intent2.putExtra("number",number_item);
                intent2.putExtra("nameLine",name_line);
                startActivityForResult(intent2, 1);

            }
        });
        back_array.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

//        refresh_activity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//                overridePendingTransition( 0, 0);
//                startActivity(getIntent());
//                overridePendingTransition( 0, 0);
//            }
//        });
    }
}
