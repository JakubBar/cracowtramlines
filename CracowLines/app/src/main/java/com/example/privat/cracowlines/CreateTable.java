package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Privat on 29.12.2017.
 */

public class CreateTable {
    private Context context;
    private Random rm;
    private String mode;
    private String nameLineSelected;
    private TableLayout tl;
    private Activity ac;
    public Integer i;
    private ArrayList<HashMap<String, String>> departuresList;
    ArrayList<HashMap<String, String>> oldStopsList;
    ArrayList<HashMap<String, String>> actualStopsList;
    ArrayList<HashMap<String, String>> infoList;

    public CreateTable(Context _c ,Random _r , String _m, TableLayout _tl, ArrayList<HashMap<String,String>> _dl,String _n,Activity _ac){
        this.context=_c;
        this.rm = _r;
        this.mode = _m;
        this.tl = _tl;
        this.departuresList = _dl;
        this.i = 0;
        this.nameLineSelected = _n;
        this.ac=_ac;
    }

    public CreateTable(Context _c ,Random _r , String _m, TableLayout _tl, Activity _ac,
                       ArrayList<HashMap<String,String>> _il,
                       ArrayList<HashMap<String,String>> _al,
                       ArrayList<HashMap<String,String>> _ol){
        this.context=_c;
        this.rm = _r;
        this.mode = _m;
        this.tl = _tl;
        this.infoList = _il;
        this.actualStopsList = _al;
        this.oldStopsList = _ol;
        this.ac=_ac;

    }
    public void getHeader(){
        TableRow tr_head = new TableRow(context);
        tr_head.setBackgroundColor(context.getColor(R.color.light_blue));
        tr_head.setId(rm.nextInt());
        tr_head.setPadding(8,8,8,8);

        tr_head.addView(getTextView("LINE",context.getColor(R.color.grey)));
        tr_head.addView(getTextView(mode == "arrival"? "FROM DIRECTION" : "DIRECTION",context.getColor(R.color.grey)));
        tr_head.addView(getTextView("TIME",context.getColor(R.color.grey)));

        tl.addView(tr_head, new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT));
    }

    public void getBody() {
        for (i = 0; i < departuresList.size(); i++) {
            TableRow tr = new TableRow(context);
            //if(departuresList.get(i).get("line").equals(nameLineSelected)){
            //    tr.setBackgroundColor(context.getColor(R.color.light_yellow));
            //}else{
                tr.setBackgroundColor(context.getColor(R.color.grey));
            //}

            tr.setId(rm.nextInt());
            tr.setPadding(8, 8, 8, 8);
            tr.setTag(departuresList.get(i).get("tripId"));

            tr.addView(getTextView(departuresList.get(i).get("line"),context.getColor(R.color.white)));
            tr.addView(getTextView(departuresList.get(i).get("direction"),context.getColor(R.color.white)));
            tr.addView(getTextView(departuresList.get(i).get("time"),context.getColor(R.color.white)));

            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tripId = v.getTag().toString();
                    Intent intent = new Intent(context, TripActivity.class);
                    intent.putExtra("tripId", tripId);
                    //context.startActivity(intent);
                    ac.startActivityForResult(intent, 1);
                }
            });


            tl.addView(tr, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT));
        }
    }

    public void getTripHeader(){
        TableRow tr_head = new TableRow(context);
        tr_head.setBackgroundColor(context.getColor(R.color.light_blue));
        tr_head.setId(rm.nextInt());
        tr_head.setPadding(8,8,8,8);

        tr_head.addView(getTextView("Time",context.getColor(R.color.grey)));
        tr_head.addView(getTextView("     ",context.getColor(R.color.grey)));
        tr_head.addView(getTextView("STOP NAME",context.getColor(R.color.grey)));

        tl.addView(tr_head, new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT));
    }

    public void getTripBody() {
        for (Integer i = 0; i < oldStopsList.size(); i++) {
            TableRow tr = new TableRow(context);
            //tr.setBackgroundColor(context.getColor(R.color.light_red));
            tr.setId(rm.nextInt());
            tr.setPadding(8, 8, 8, 8);
            tr.setTag(oldStopsList.get(i).get("number"));

            tr.addView(getTextView(oldStopsList.get(i).get("actualTime"),context.getColor(R.color.white)));
            tr.addView(getImageView("middle_stop_old"));
            tr.addView(getTextView(oldStopsList.get(i).get("nameStop"),context.getColor(R.color.white)));

            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = v.getTag().toString();
                    Intent intent = new Intent(context, StopSelectedActivity.class);
                    intent.putExtra("number", number);
                    //context.startActivity(intent);
                    ac.startActivityForResult(intent, 1);
                }
            });

            tl.addView(tr, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT));
        }

        for (Integer i = 0; i < actualStopsList.size(); i++) {
            TableRow tr = new TableRow(context);
            String actualTime = actualStopsList.get(i).get("actualTime");
            String typeStops = "middle_stop";
            if(actualStopsList.get(i).get("status").equals("STOPPING")){
                actualTime = "NOW";
                typeStops = "middle_stop_actual";
            }else{
                actualTime = checkActualTime(actualStopsList.get(i).get("actualTime"));
            }

            tr.setId(rm.nextInt());
            tr.setPadding(8, 8, 8, 8);
            tr.setTag(actualStopsList.get(i).get("number"));

            tr.addView(getTextView(actualTime,context.getColor(R.color.white)));
            tr.addView(getImageView(typeStops));
            tr.addView(getTextView(actualStopsList.get(i).get("nameStop"),context.getColor(R.color.white)));

            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = v.getTag().toString();
                    Intent intent = new Intent(context, StopSelectedActivity.class);
                    intent.putExtra("number", number);
                    //context.startActivity(intent);
                    ac.startActivityForResult(intent, 1);
                }
            });

            tl.addView(tr, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT));
        }
    }

    public TextView getTextView(String text, int colorText){
        TextView tmp = new TextView(context);
        tmp.setId(rm.nextInt());
        tmp.setText(text);
        tmp.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1));
        tmp.setTextColor(colorText);
        tmp.setGravity(Gravity.CENTER);

        return tmp;
    }

    public ImageView getImageView(String type){
        ImageView tmp = new ImageView(context);
        tmp.setId(rm.nextInt());
        tmp.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1));
        switch (type) {
            case "middle_stop":
                tmp.setImageResource(R.drawable.stop_middle);
                break;

            case "middle_stop_actual":
                tmp.setImageResource(R.drawable.stop_middle_actual);
                break;
            case "middle_stop_old":
                tmp.setImageResource(R.drawable.stop_middle_old);
                break;

            default:
        }

        return tmp;
    }

    public String checkActualTime(String actualTime){
        DateFormat df = new SimpleDateFormat("HH:mm");
        String nowTime = df.format(Calendar.getInstance().getTime());
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = sdf.parse(nowTime);
            d2 = sdf.parse(actualTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long elapsed = d2.getTime() - d1.getTime();
        if(elapsed == 0 || elapsed < 0) return "< 1 Min";
        return (elapsed / minutesInMilli) + " Min";
    }

}
