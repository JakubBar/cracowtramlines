package com.example.privat.cracowlines;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.CallLog;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ThemedSpinnerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Privat on 31.12.2017.
 */

public class ListAdapter extends BaseAdapter implements Filterable, ThemedSpinnerAdapter {
    private LayoutInflater mInflater;

    private int[] mTo;
    private String[] mFrom;
    private android.widget.SimpleAdapter.ViewBinder mViewBinder;

    private List<? extends Map<String, ?>> mData;
    private Context context;
    private int mResource;
    private int mDropDownResource;
    private Activity ac;

    private LayoutInflater mDropDownInflater;

    private SimpleFilter mFilter;
    private ArrayList<Map<String, ?>> mUnfilteredData;

    public ListAdapter(Context _context, List<? extends Map<String, ?>> data,
                       int resource, String[] from, int[] to, Activity _ac) {
        mData = data;
        mResource = mDropDownResource = resource;
        mFrom = from;
        mTo = to;
        mInflater = null;
        context = _context;
        ac=_ac;
    }

    public int getCount() {
        return mData.size();
    }

    public Object getItem(int position) {
        return mData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return createViewFromResource(mInflater, position, convertView, parent, mResource);
    }

    private View createViewFromResource(LayoutInflater inflater, int position, View convertView,
                                        ViewGroup parent, int resource) {
        View v;
        if (convertView == null) {
            v = inflater.inflate(resource, parent, false);
        } else {
            v = convertView;
        }
        TextView tv = (TextView)v.findViewById(R.id.name_stops);
        if(tv == null){
            tv = (TextView)v.findViewById(R.id.name_lines);
            final TextView finalTv = tv;
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id_item = v.getTag().toString();
                    Intent intent = new Intent(context, RouteActivity.class);
                    intent.putExtra("routeId", id_item);
                    intent.putExtra("nameLine", finalTv.getText());
                    ac.startActivityForResult(intent,1);
                    //context.startActivity(intent);
                }
            });
        }else{
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number_item = v.getTag().toString();
                    Intent intent = new Intent(context, StopSelectedActivity.class);
                    intent.putExtra("number", number_item);
                    ac.startActivityForResult(intent,1);
                    //context.startActivity(intent);
                }
            });
            ImageView imageView=(ImageView)v.findViewById(R.id.favourites);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = v.getTag().toString();
                    ImageView iv = (ImageView)v.findViewById(v.getId());
                    DBHelper dbHelper = new DBHelper(context);
                    if(dbHelper.checkFavouritesStops(number)){
                        dbHelper.deleteFavouritesStop(number);
                        iv.setImageResource(0);
                        iv.setImageResource(R.drawable.fav_alone);
                    }else {
                        dbHelper.insertFavouritesStop(v.getTag().toString());
                        iv.setImageResource(0);
                        iv.setImageResource(R.drawable.fav_with);
                    }
                }
            });
        }


        bindView(position, v);

        return v;
    }

    public void setDropDownViewResource(int resource) {
        mDropDownResource = resource;
    }

    @Override
    public void setDropDownViewTheme(Resources.Theme theme) {
        if (theme == null) {
            mDropDownInflater = null;
        } else if (theme == mInflater.getContext().getTheme()) {
            mDropDownInflater = mInflater;
        } else {
            final Context context = new ContextThemeWrapper(mInflater.getContext(), theme);
            mDropDownInflater = LayoutInflater.from(context);
        }
    }

    @Override
    public Resources.Theme getDropDownViewTheme() {
        return mDropDownInflater == null ? null : mDropDownInflater.getContext().getTheme();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = mDropDownInflater == null ? mInflater : mDropDownInflater;
        return createViewFromResource(inflater, position, convertView, parent, mDropDownResource);
    }

    private void bindView(int position, View view) {
        final Map dataSet = mData.get(position);
        if (dataSet == null) {
            return;
        }

        final android.widget.SimpleAdapter.ViewBinder binder = mViewBinder;
        final String[] from = mFrom;
        final int[] to = mTo;
        final int count = to.length;

        for (int i = 0; i < count; i++) {
            final View v = view.findViewById(to[i]);
            if (v != null) {
                final Object data = dataSet.get(from[i]);
                String text = data == null ? "" : data.toString();
                if (text == null) {
                    text = "";
                }

                boolean bound = false;
                if (binder != null) {
                    bound = binder.setViewValue(v, data, text);
                }

                if (!bound) {
                    if (v instanceof Checkable) {
                        if (data instanceof Boolean) {
                            ((Checkable) v).setChecked((Boolean) data);
                        } else if (v instanceof TextView) {
                            // Note: keep the instanceof TextView check at the bottom of these
                            // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                            setViewText((TextView) v, text,dataSet.get(from[0]).toString());
                        } else {
                            throw new IllegalStateException(v.getClass().getName() +
                                    " should be bound to a Boolean, not a " +
                                    (data == null ? "<unknown type>" : data.getClass()));
                        }
                    } else if (v instanceof TextView) {
                        // Note: keep the instanceof TextView check at the bottom of these
                        // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                        setViewText((TextView) v, text,dataSet.get(from[0]).toString());
                    } else if (v instanceof ImageView) {
                        if (data instanceof Integer) {
                            setViewImage((ImageView) v, (Integer) data);
                        } else {
                            setViewImage((ImageView) v, text);
                        }
                    } else {
                        throw new IllegalStateException(v.getClass().getName() + " is not a " +
                                " view that can be bounds by this SimpleAdapter");
                    }
                }
            }
        }
    }

    public android.widget.SimpleAdapter.ViewBinder getViewBinder() {
        return mViewBinder;
    }

    public void setViewBinder(android.widget.SimpleAdapter.ViewBinder viewBinder) {
        mViewBinder = viewBinder;
    }

    public void setViewImage(ImageView v, int value) {
        v.setImageResource(value);
    }


    public void setViewImage(ImageView v, String number) {
        try {
            DBHelper dbHelper = new DBHelper(context);
            if(!dbHelper.checkFavouritesStops(number)){
                v.setImageResource(0);
                v.setImageResource(R.drawable.fav_alone);
            }else {
                v.setImageResource(0);
                v.setImageResource(R.drawable.fav_with);
            }
            v.setTag(number);
        } catch (NumberFormatException nfe) {
            //v.setImageURI(Uri.parse(value));
        }
    }

    public void setViewText(TextView v, String text,String tag) {
        v.setText(text);
        v.setTag(tag);
    }

    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new SimpleFilter();
        }
        return mFilter;
    }

    public static interface ViewBinder {
        boolean setViewValue(View view, Object data, String textRepresentation);
    }

    private class SimpleFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mUnfilteredData == null) {
                mUnfilteredData = new ArrayList<Map<String, ?>>(mData);
            }

            if (prefix == null || prefix.length() == 0) {
                ArrayList<Map<String, ?>> list = mUnfilteredData;
                results.values = list;
                results.count = list.size();
            } else {
                String prefixString = prefix.toString().toLowerCase();

                ArrayList<Map<String, ?>> unfilteredValues = mUnfilteredData;
                int count = unfilteredValues.size();

                ArrayList<Map<String, ?>> newValues = new ArrayList<Map<String, ?>>(count);

                for (int i = 0; i < count; i++) {
                    Map<String, ?> h = unfilteredValues.get(i);
                    if (h != null) {

                        int len = mTo.length;

                        for (int j=0; j<len; j++) {
                            String str =  (String)h.get(mFrom[j]);

                            String[] words = str.split(" ");
                            int wordCount = words.length;

                            for (int k = 0; k < wordCount; k++) {
                                String word = words[k];

                                if (word.toLowerCase().startsWith(prefixString)) {
                                    newValues.add(h);
                                    break;
                                }
                            }
                        }
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mData = (List<Map<String, ?>>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
