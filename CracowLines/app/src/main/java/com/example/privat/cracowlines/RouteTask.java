package com.example.privat.cracowlines;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Privat on 28.12.2017.
 */

public class RouteTask  extends AsyncTask<Void, String, Void> {
    private Context context;
    private ListView lv;
    private String routeId;
    private String nameLine;
    private Activity ac;
    private ProgressDialog progressDialog ;
    ArrayList<HashMap<String, String>> stopsList;
    private Boolean dismiss = true;

    RouteTask(Context _context, ListView _lv, String _routeId, String _nameLine, Activity _ac) {
        context = _context;
        routeId = _routeId;
        lv = _lv;
        ac=_ac;
        nameLine = _nameLine;
        stopsList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(ac);
        progressDialog.setTitle("Downloading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler(context);
            if(!sh.isNetworkAvailable()){
                publishProgress("Network is unavailable.");
                dismiss=false;
            }
            String url = "http://www.ttss.krakow.pl/internetservice/services/routeInfo/routeStops?routeId=" + routeId;
            String jsonStr = sh.makeServiceCall(url);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONArray stopsJSON = jsonObj.getJSONArray("stops");
                    for (int i = 0; i < stopsJSON.length(); i++) {
                        Boolean tmp = false;
                        JSONObject c = stopsJSON.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String number = c.getString("number");
/*                        if(!tmp) {
                            String url2 = "http://www.ttss.krakow.pl/internetservice/services/passageInfo/stopPassages/stop?stop=" + number + "&mode=arrival";
                            String jsonStr2 = sh.makeServiceCall(url2);
                            if (jsonStr2 != null) {
                                try {
                                    JSONObject jsonObj2 = new JSONObject(jsonStr2);
                                    JSONArray stopsJSON2 = jsonObj2.getJSONArray("actual");
                                    for (int j = 0; j < stopsJSON2.length(); j++) {
                                        JSONObject c2 = stopsJSON2.getJSONObject(j);
                                        if (c2.getString("patternText").equals(nameLine)) {
                                            tmp = true;
                                            continue;
                                        }
                                    }
                                } catch (final JSONException e) {
                                }

                            } else {
                            }
                        }
                        if(!tmp) continue;*/
                        HashMap<String, String> stops = new HashMap<>();
                        stops.put("id", id);
                        stops.put("name", name);
                        stops.put("number", number);
                        stops.put("nameLineSelected", nameLine);
                        stopsList.add(stops);
                    }
                } catch (final JSONException e) {
                }

            } else {}
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        progressDialog.setTitle("Error");

        progressDialog.setMessage(values[0]);

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing() && dismiss) {
            progressDialog.dismiss();
        }
        ListAdapter adapter = new SimpleAdapter(context, stopsList,
                R.layout.route_list_item, new String[]{"number", "name","nameLineSelected"},
                new int[]{R.id.id_stops, R.id.name_stops,R.id.name_line});
        lv.setAdapter(adapter);
    }
}