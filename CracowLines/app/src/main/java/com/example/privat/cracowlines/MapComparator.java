package com.example.privat.cracowlines;

import java.util.Comparator;
import java.util.Map;

/**
 * Created by Privat on 28.12.2017.
 */

class MapComparator implements Comparator<Map<String, String>>
{
    private final String key;
    private Boolean isInteger;

    public MapComparator(String key,Boolean isInt)
    {
        this.key = key;
        this.isInteger = isInt;
    }

    public int compare(Map<String, String> first,
                       Map<String, String> second)
    {
        if(isInteger) {
            Integer firstValue = Integer.parseInt(first.get(key));
            Integer secondValue = Integer.parseInt(second.get(key));
            return Integer.compare(firstValue, secondValue);
        }else{
            String firstValue = first.get(key);
            String secondValue = second.get(key);
            return firstValue.compareTo(secondValue);
        }
    }
}
